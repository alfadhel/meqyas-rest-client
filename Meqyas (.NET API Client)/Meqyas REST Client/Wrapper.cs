﻿using System;
using System.Collections.Generic;
using Meqyas.Model;

namespace Meqyas.API.REST
{
    class ResultWrapperForAccount
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Account result { get; set; }
        public ResultWrapperForAccount() { }
    }
    class ResultWrapperForAccountList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Account> result { get; set; }
        public ResultWrapperForAccountList() { }
    }
    class ResultWrapperForBranch
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Branch result { get; set; }
        public ResultWrapperForBranch() { }
    }
    class ResultWrapperForBranchList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Branch> result { get; set; }
        public ResultWrapperForBranchList() { }
    }
    class ResultWrapperForBrand
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Brand result { get; set; }
        public ResultWrapperForBrand() { }
    }
    class ResultWrapperForBrandList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Brand> result { get; set; }
        public ResultWrapperForBrandList() { }
    }
    class ResultWrapperForCompany
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Company result { get; set; }
        public ResultWrapperForCompany() { }
    }
    class ResultWrapperForCompanyList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Company> result { get; set; }
        public ResultWrapperForCompanyList() { }
    }
    class ResultWrapperForCurrency
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Currency result { get; set; }
        public ResultWrapperForCurrency() { }
    }
    class ResultWrapperForCurrencyList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Currency> result { get; set; }
        public ResultWrapperForCurrencyList() { }
    }
    class ResultWrapperForEmailTemplate
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public EmailTemplate result { get; set; }
        public ResultWrapperForEmailTemplate() { }
    }
    class ResultWrapperForEmailTemplateList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<EmailTemplate> result { get; set; }
        public ResultWrapperForEmailTemplateList() { }
    }
    class ResultWrapperForHTMLPage
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public HTMLPage result { get; set; }
        public ResultWrapperForHTMLPage() { }
    }
    class ResultWrapperForHTMLPageList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<HTMLPage> result { get; set; }
        public ResultWrapperForHTMLPageList() { }
    }
    class ResultWrapperForNews
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public News result { get; set; }
        public ResultWrapperForNews() { }
    }
    class ResultWrapperForNewsList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<News> result { get; set; }
        public ResultWrapperForNewsList() { }
    }
    class ResultWrapperForOffer
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Offer result { get; set; }
        public ResultWrapperForOffer() { }
    }
    class ResultWrapperForOfferList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Offer> result { get; set; }
        public ResultWrapperForOfferList() { }
    }
    class ResultWrapperForOfferReview 
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public OfferReview  result { get; set; }
        public ResultWrapperForOfferReview () { }
    }
    class ResultWrapperForOfferReviewList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<OfferReview > result { get; set; }
        public ResultWrapperForOfferReviewList() { }
    }
    class ResultWrapperForProduct 
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Product  result { get; set; }
        public ResultWrapperForProduct () { }
    }
    class ResultWrapperForProductList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Product > result { get; set; }
        public ResultWrapperForProductList() { }
    }
    class ResultWrapperForProductReview
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public ProductReview result { get; set; }
        public ResultWrapperForProductReview() { }
    }
    class ResultWrapperForProductReviewList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<ProductReview> result { get; set; }
        public ResultWrapperForProductReviewList() { }
    }
    class ResultWrapperForProductType
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public ProductType result { get; set; }
        public ResultWrapperForProductType() { }
    }
    class ResultWrapperForProductTypeList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<ProductType> result { get; set; }
        public ResultWrapperForProductTypeList() { }
    }
    class ResultWrapperForRole
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Role result { get; set; }
        public ResultWrapperForRole() { }
    }
    class ResultWrapperForRoleList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Role> result { get; set; }
        public ResultWrapperForRoleList() { }
    }
    class ResultWrapperForSetting
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Setting result { get; set; }
        public ResultWrapperForSetting() { }
    }
    class ResultWrapperForSettingList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Setting> result { get; set; }
        public ResultWrapperForSettingList() { }
    }
    class ResultWrapperForForm 
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public Form  result { get; set; }
        public ResultWrapperForForm () { }
    }
    class ResultWrapperForFormList
    {
        public Boolean status { get; set; }
        public int code { get; set; }
        public List<Form > result { get; set; }
        public ResultWrapperForFormList() { }
    }
}
