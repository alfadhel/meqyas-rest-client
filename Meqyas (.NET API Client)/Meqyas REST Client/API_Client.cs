﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Resources;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Meqyas.API.Exceptions;
using Meqyas.Model;

namespace Meqyas.API.REST
{
    public class ApiClient
    {
        private String apiKey;
        private System.Resources.ResourceManager rm;
        public ApiClient()
        {
            try
            {
                this.apiKey = ConfigurationManager.AppSettings["apiKey"];
            }
            catch (Exception e)
            {
                throw new NoApiKeyException();
            }
            if (String.IsNullOrEmpty(this.apiKey))
                throw new NoApiKeyException();
            rm = Meqyas.API.REST.Properties.Resources.ResourceManager;
        }

        public Account authenticate(String screenName, String password)
        {
            RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceLogin"));
		    List<RestPostParameter> postData = new List<RestPostParameter>();
		    postData.Add(new RestPostParameter("screenName", screenName));
		    postData.Add(new RestPostParameter("password", password));
		    client.postData = postData;
		    client.method = RestClient.HttpVerb.POST;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}", this.apiKey));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForAccount resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForAccount>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
        public Account Register(String email, String screenName, String password, String fullName, char gender, String timeZone)
        {
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceRegister"));
		    List<RestPostParameter> postData = new List<RestPostParameter>();
		    postData.Add(new RestPostParameter("email", email));
		    postData.Add(new RestPostParameter("screenName", screenName));
		    postData.Add(new RestPostParameter("password", password));
		    postData.Add(new RestPostParameter("fullName", fullName));
		    postData.Add(new RestPostParameter("gender", gender));
            postData.Add(new RestPostParameter("language", Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2)));
		    postData.Add(new RestPostParameter("timeZone", timeZone));
		    client.postData = postData;
		    client.method = RestClient.HttpVerb.POST;
		    String jsonStr = client.makeRequest(String.Format("?apiKey={0}", this.apiKey));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
			    ResultWrapperForAccount resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForAccount>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }

        public List<Product> serachForProduct(String query, int count, long last_id)
        {
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceSearch") + rm.GetString("ServiceProduct"));
		    client.method = RestClient.HttpVerb.GET;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}&last_id={1}&lang={2}&count={3}&query={4}",
                    this.apiKey, last_id, Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2), count, query));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForProductList resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForProductList>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
	
	    public Product getProductDetails(long id)
        {
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceProductDetails") + "/" + id);
		    client.method = RestClient.HttpVerb.GET;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}&lang={1}",
                    this.apiKey, Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2)));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForProduct resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForProduct>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
	
	    public List<Offer> getProductOffers(long product_id, int count, long last_id){
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceOffer"));
		    client.method = RestClient.HttpVerb.GET;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}&product_id={1}&last_id={2}&lang={3}&count={4}",
                    this.apiKey, product_id, last_id, Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2), count));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForOfferList resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForOfferList>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
	
	    public List<OfferReview> getOfferReviews(long offer_id, int count, long last_id){
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceOfferReview") + "/list");
		    client.method = RestClient.HttpVerb.GET;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}&last_id={1}&lang={2}&count={3}&offer_id={4}",
                    this.apiKey, last_id, Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2), count, offer_id));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForOfferReviewList resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForOfferReviewList>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
	
	    public OfferReview addReview(long offer_id, String userToken, String title, String content, int ratting){
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceOfferReview") + "/add");
		    List<RestPostParameter> postData = new List<RestPostParameter>();
		    postData.Add(new RestPostParameter("offer_id", offer_id));
		    postData.Add(new RestPostParameter("token", userToken));
		    postData.Add(new RestPostParameter("title", title));
		    postData.Add(new RestPostParameter("content", content));
		    postData.Add(new RestPostParameter("ratting", ratting));
            postData.Add(new RestPostParameter("lang", Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2)));
		    client.postData = postData;
		    client.method = RestClient.HttpVerb.POST;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}", this.apiKey));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForOfferReview resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForOfferReview>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
	
	    public OfferReview editReview(long id, String userToken, String title, String content, int ratting){
		    RestClient client = new RestClient(rm.GetString("EndPoint") + 
				    rm.GetString("ServiceOfferReview") + "/edit");
		    List<RestPostParameter> postData = new List<RestPostParameter>();
		    postData.Add(new RestPostParameter("id", id));
		    postData.Add(new RestPostParameter("token", userToken));
		    postData.Add(new RestPostParameter("title", title));
		    postData.Add(new RestPostParameter("content", content));
		    postData.Add(new RestPostParameter("ratting", ratting));
		    client.postData = postData;
		    client.method = RestClient.HttpVerb.POST;
            String jsonStr = client.makeRequest(String.Format("?apiKey={0}", this.apiKey));
		    var values = JObject.Parse(jsonStr);
		    if((Boolean)values["status"]){
                ResultWrapperForOfferReview resultWrapper = JsonConvert.DeserializeObject<ResultWrapperForOfferReview>(jsonStr);
			    return resultWrapper.result;
		    }else{
			    if((int)values["code"] == 403)
				    throw new ApiKeyNotValidException();
			    throw new Exception(values["result"].ToString());
		    }
	    }
    }
}
