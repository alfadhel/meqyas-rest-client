using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Meqyas.Model
{
	public class Account
	{
		public Int64 id { get; set;}
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime createDate { get; set; }
		public String screenName { get; set;}
		public String email { get; set;}
		public String language { get; set;}
		public String timeZone { get; set;}
		public String fullName { get; set;}
		public char gender { get; set;}
		public Company company { get; set;}
		public Dictionary<String, Boolean> actions { get; set;}
		//Token used with Apache Jersey
        public String token { get; set; }
        public Account() { }
	}
    public class Action
    {
        public String name { get; set; }
        public ActionGroup group { get; set; }
        public Boolean defaultValue { get; set; }
        public int order { get; set; }
        public Action() { }
    }
    public class ActionGroup
    {
        public String name { get; set; }
        public int order { get; set; }
        public List<Action> actions { get; set; }
        public ActionGroup() { }
    }
    public class Application
    {
        public String key { get; set; }
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime date { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String website { get; set; }
        public Account owner { get; set; }
        public Application() { }
    }
    public class Branch 
    {
        public Int64 id { get; set; }
        public Company company { get; set; }
        public String location { get; set; }
        public String defaultLang { get; set; }
        public BranchLocal local { get; set; }
        public Branch() { }
    }
    public class BranchLocal 
    {
        public String lang { get; set; }
        public String name { get; set; }
        public String contact { get; set; }
        public String description { get; set; }
        public BranchLocal() { }
    }
    public class Brand 
    {
        public Int64 id { get; set; }
        public String name { get; set; }
        public String defaultLang { get; set; }
        public BrandLocal local { get; set; }
        public Brand() { }
    }
    public class BrandLocal {
        public String lang { get; set; }
        public String name { get; set; }
        public BrandLocal() { }
    }
	public class Company
	{
		public Int64 id { get; set;}
		public String name { get; set;}
        public String defaultLang { get; set; }
		public CompanyLocal local { get; set;}
        public Company() { }
	}
	public class CompanyLocal
	{
		public String lang { get; set;}
		public String name { get; set;}
		public String description { get; set;}
        public CompanyLocal() { }
	}
    public class Content
    {
        public Int64 id { get; set; }
        public Account author { get; set; }
        public String title { get; set; }
        public String imgUrl { get; set; }
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime date { get; set; }
        public ContentLocal local { get; set; }
        public String instance { get; set; }
        public Content() { }
    }
    public class ContentLocal
    {
        public String lang { get; set; }
        public String title { get; set; }
        public String head { get; set; }
        public String body { get; set; }
        public List<String> tagList { get; set; }
        public ContentLocal() { }
    }
    public class Currency
    {
        public String code { get; set; }
        public String name { get; set; }
        public String defaultLang { get; set; }
        public CurrencyLocal local { get; set; }
        public Currency() { }
    }
    public class CurrencyLocal
    {
        public String lang { get; set; }
        public String name { get; set; }
        public CurrencyLocal() { }
    }
    public class EmailTemplate 
    {
        public Int64 id { get; set; }
        public String name { get; set; }
        public String defaultLang { get; set; }
        public EmailTemplateLocal local { get; set; }
        public EmailTemplate() { }
    }
    public class EmailTemplateLocal
    {
        public String lang { get; set; }
        public String name { get; set; }
        public String title { get; set; }
        public String body { get; set; }
        public EmailTemplateLocal() { }
    }
    public class Feature
    {
        public Int64 id { get; set; }
        public String name { get; set; }
        public String inputType { get; set; }
        public String inputValues { get; set; }
        public Boolean required { get; set; }
        public int order { get; set; }
        public FeatureLocal local { get; set; }
        public Feature() { }
    }
    public class FeatureLocal
    {
        public String lang { get; set; }
        public String label { get; set; }
        public String inputLabels { get; set; }
        public FeatureLocal() { }
    }
    public class HTMLPage 
    {
        public Int64 id { get; set; }
        public String name { get; set; }
        public String url { get; set; }
        public String defaultLang { get; set; }
        public Account creator { get; set; }
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime createDate { get; set; }
        public HTMLPageLocal local { get; set; }
        public HTMLPage() { }
    }
    public class HTMLPageLocal
    {
        public String lang { get; set; }
        public String title { get; set; }
        public String htmlTitle { get; set; }
        public String content { get; set; }
        public String style { get; set; }
        public String script { get; set; }
        public HTMLPageLocal() { }
    }
    public class News : Content
    {
        public News() { }
    }
    public class NewsLocal : ContentLocal
    {
        public NewsLocal() {}
    }
    public class Offer
    {
        public Int64 id { get; set; }
        public Product product { get; set; }
        public Company company { get; set; }
        public Double price { get; set; }
        public Currency currency { get; set; }
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime fromDate { get; set; }
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime toDate { get; set; }
        public List<Product> additionalProducts { get; set; }
        public List<Branch> branchs { get; set; }
        public String defaultLang { get; set; }
        public OfferLocal local { get; set; }
        public Offer() { }
    }
    public class OfferLocal
    {
        public String lang { get; set; }
        public String description { get; set; }
        public OfferLocal() { }
    }
    public class OfferReview
    {
        public Int64 id { get; set; }
        public Offer offer { get; set; }
        public String lang { get; set; }
        public Account writer { get; set; }
        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime date { get; set; }
        public String title { get; set; }
        public String content { get; set; }
        // out of 5
        public int rating { get; set; }
        public OfferReview() { }
    }
    public class Product
    {
        public Int64 id { get; set; }
        public String name { get; set; }
        public String modelNo { get; set; }
        public String number { get; set; }
        public ProductType type { get; set; }
        public Brand brand { get; set; }
        public List<ProductFeature> features { get; set; }
        public Product() { }
    }
    public class ProductFeature
    {
        public Int64 id { get; set; }
        public Feature feature { get; set; }
        public String value { get; set; }
        public ProductFeature() { }
    }
    public class ProductPoll
    {
        public Int64 id { get; set; }
        public Product product { get; set; }
        public Account voter { get; set; }
        public int design { get; set; }
        public int features { get; set; }
        public int performance { get; set; }
        public ProductPoll() { }
    }
    public class ProductReview : Content {
        public Product product { get; set; }
        public ProductReview() { }
    }
    public class ProductReviewLocal : ContentLocal 
    {
        public ProductReviewLocal() { }
    }
    public class ProductType {
        public Int64 id { get; set; }
        public String name { get; set; }
        public ProductType parent { get; set; }
        public String defaultLang { get; set; }
        public ProductTypeLocal local { get; set; }
        public ProductType() { }
    }
    public class ProductTypeLocal
    {
        public String lang { get; set; }
        public String name { get; set; }
        public ProductTypeLocal() { }
    }
    public class Role
    {
        public Int64 id { get; set; }
        public String name { get; set; }
        public List<RoleAction> actions { get; set; }
        public Role() { }
    }
    public class RoleAction
    {
        public Int64 id { get; set; }
        public Role role { get; set; }
        public Action action { get; set; }
        public Boolean value { get; set; }
        public RoleAction() { }
    }
    public class Setting 
    {
        public String key { get; set; }
        public SettingGroup group { get; set; }
        public int order { get; set; }
        public String inputType { get; set; }
        public String inputValues { get; set; }
        public String value { get; set; }
        public String dataType { get; set; }
        public String unit { get; set; }
        public Setting() { }
    }
    public class SettingGroup
    {
        public String name { get; set; }
        public int order { get; set; }
        public List<Setting> settings { get; set; }
        public SettingGroup() { }
    }
    //Fix Unix date.
    //Date is sent as milliseconds (integer) 
    class UnixDateTimeConverter : DateTimeConverterBase
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType != JsonToken.Integer)
            {
                throw new Exception(
                    String.Format("Unexpected token parsing date. Expected Integer, got {0}.",
                    reader.TokenType));
            }

            var ticks = (long)reader.Value;

            var date = new DateTime(1970, 1, 1);
            date = date.AddMilliseconds(ticks);

            return date;
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            long ticks;
            if (value is DateTime)
            {
                var epoc = new DateTime(1970, 1, 1);
                var delta = ((DateTime)value) - epoc;
                if (delta.TotalSeconds < 0)
                {
                    throw new ArgumentOutOfRangeException(
                        "Unix epoc starts January 1st, 1970");
                }
                ticks = (long)delta.TotalMilliseconds;
            }
            else
            {
                throw new Exception("Expected date object value.");
            }
            writer.WriteValue(ticks);
        }
    }
}