﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.Generic;

namespace Meqyas.API.REST
{
    class RestClient
    {
        public enum HttpVerb
        {
            GET, POST, PUT, DELETE
        }

        public string endPoint { get; set; }
        public HttpVerb method { get; set; }
        public string contentType { get; set; }
        public List<RestPostParameter> postData { get; set; }
        public string accept { get; set; }

        public RestClient()
        {
            this.method = HttpVerb.GET;
            this.contentType = "text/xml";
            this.accept = "application/json";
        }

        public RestClient(String endPoint)
        {
            this.endPoint = endPoint;
            this.method = HttpVerb.GET;
            this.contentType = "text/xml";
            this.accept = "application/json";
        }

        public RestClient(String endPoint, HttpVerb method)
        {
            this.endPoint = endPoint;
            this.method = method;
            this.contentType = "text/xml";
            this.accept = "application/json";
        }

        public RestClient(String endPoint, HttpVerb method, List<RestPostParameter> postData)
        {
            this.endPoint = endPoint;
            this.method = method;
            this.postData = postData;
            this.contentType = "text/xml";
            this.accept = "application/json";
        }

        public string makeRequest()
        {
            return makeRequest("");
        }

        public string makeRequest(string parameters)
        {
            var request = (HttpWebRequest)WebRequest.Create(this.endPoint + parameters);

            request.Method = this.method.ToString();
            request.ContentLength = 0;
            request.ContentType = this.contentType;
            request.Accept = this.accept;

            if (this.postData != null && this.method == HttpVerb.POST)
            {
                StringBuilder input = new StringBuilder();
                foreach (RestPostParameter parameter in this.postData)
                {
                    if (parameter.value == null)
                        continue;
                    if (input.Length != 0)
                        input.Append("&");
                    input.Append(parameter.name).Append("=").Append(parameter.value);
                }
                var encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("UTF-8").GetBytes(input.ToString());
                request.ContentLength = bytes.Length;

                using (var writeStream = request.GetRequestStream())
                {
                    writeStream.Write(bytes, 0, bytes.Length);
                }
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                    throw new Exception(message);
                }

                // grab the response
                using (var responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                }

                return responseValue;
            }
        }

    }
    class RestPostParameter
    {
        public String name { get; set; }
        public Object value { get; set; }

        public RestPostParameter(String name, Object value)
        {
            this.name = name;
            this.value = value;
        }
    }
}
