﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meqyas.API.Exceptions
{
    public class NoApiKeyException : System.Exception
    {
        public NoApiKeyException() : base("API Key (apiKey) not found. Please add it in 'api.config' file") { }
    }

    public class ApiKeyNotValidException : System.Exception
    {
        public ApiKeyNotValidException() : base("API Key not valid") { }
    }

    public class AccountNotFoundException : System.Exception
    {
        public AccountNotFoundException() : base("Account not found") { }
    }
    public class InvalidParameterException : SystemException 
    {
        public InvalidParameterException(String msg) : base(msg) { }
    }
    public class MissingParameterException : Exception 
    {
        public MissingParameterException(String msg) : base(msg) { }
    }
    public class NotFoundException : Exception 
    {
        public NotFoundException(String msg) : base(msg) { }
    }
    public class PrivilegeException : Exception 
    {
        public PrivilegeException(String msg) : base(msg) { }
    }
}
