﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meqyas.Util
{
    public class Form
    {
        public String name { get; set; }
        public String method { get; set; }
        public String action { get; set; }
        public List<FormElement> elements { get; set; }
        public Form() { }
    }
    public class FormElement
    {
        public enum InputType {
                text, textarea, select, radio, checkbox, date, datetime, email, month, number, range, search, tel, time, url, week, hidden,
                wysiwyg, header, label
        }
        public String name { get; set; }
        public InputType inputType { get; set; }
        public String label { get; set; }
        public String[] inputValues { get; set; }
        public String[] inputLabels { get; set; }
        public Boolean required { get; set; }
        public int length { get; set; }
        public String format { get; set; }
        public String value { get; set; }
        public String[] values { get; set; }
        public FormElement() { }
    }
}
