﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Meqyas.API.REST;
using Meqyas.Model;

namespace Meqyas.API.REST.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            ApiClient apiClient = new ApiClient();
            Account account = apiClient.authenticate("admin", "admin");
            Debug.WriteLine(account.fullName);
            Debug.WriteLine(account.createDate);
        }
    }
}
