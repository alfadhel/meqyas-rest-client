package info.meqyas.util;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Form implements Serializable {
	private String name;
	private String method;
	private String action;
	private List<FormElement> elements;
	public Form(){}
	public String getName() {
		return name;
	}
	public String getMethod() {
		return method;
	}
	public String getAction() {
		return action;
	}
	public List<FormElement> getElements() {
		return elements;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public void setElements(List<FormElement> elements) {
		this.elements = elements;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Form other = (Form) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
