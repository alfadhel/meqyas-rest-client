package info.meqyas.util;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FormElement implements Serializable {
	public enum InputType {
		text, textarea, select, radio, checkbox, date, datetime, email, month, number, range, search, tel, time, url, week, hidden,
		wysiwyg, header, label
	}
	private String name;
	private InputType inputType;
	private String label;
	private String[] inputValues;
	private String[] inputLabels;
	private boolean required;
	private int length;
	private String format;
	private String value;
	private String[] values;
	public FormElement(){}
	public String getName() {
		return name;
	}
	public InputType getInputType() {
		return inputType;
	}
	public String getLabel() {
		return label;
	}
	public String[] getInputValues() {
		return inputValues;
	}
	public String[] getInputLabels() {
		return inputLabels;
	}
	public boolean isRequired() {
		return required;
	}
	public int getLength() {
		return length;
	}
	public String getFormat() {
		return format;
	}
	public String getValue() {
		return value;
	}
	public String[] getValues() {
		return values;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setInputType(InputType inputType) {
		this.inputType = inputType;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setInputValues(String[] inputValues) {
		this.inputValues = inputValues;
	}
	public void setInputLabels(String[] inputLabels) {
		this.inputLabels = inputLabels;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setValues(String[] values) {
		this.values = values;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormElement other = (FormElement) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
