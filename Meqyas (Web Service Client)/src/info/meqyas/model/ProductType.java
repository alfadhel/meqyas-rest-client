package info.meqyas.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ProductType implements Serializable {
	private long id;
    private String name;
    private ProductType parent;
    private List<Feature> features;
    private String defaultLang;
    private ProductTypeLocal local;
	public ProductType() {
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public ProductType getParent() {
		return parent;
	}
	public List<Feature> getFeatures() {
		return features;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public ProductTypeLocal getLocal() {
		return local;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setParent(ProductType parent) {
		this.parent = parent;
	}
	public void setFeatures(List<Feature> features) {
		this.features = features;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public void setLocal(ProductTypeLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductType other = (ProductType) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
