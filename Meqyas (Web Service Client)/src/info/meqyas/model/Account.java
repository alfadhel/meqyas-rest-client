package info.meqyas.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("serial")
public class Account implements Serializable {
	private long id;
	private Date createDate = new Date();
	private String screenName;
	private String email;
	private String language;
	private String timeZone;
	private String fullName;
	private char gender;
	private Company company;
	private List<Role> roles;
	private HashMap<String, Boolean> actions;
	//Token used with Apache Jersey
	private String token;
	public Account() {
	}
	public long getId() {
		return id;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public String getScreenName() {
		return screenName;
	}
	public String getEmail() {
		return email;
	}
	public String getLanguage() {
		return language;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public String getFullName() {
		return fullName;
	}
	public char getGender() {
		return gender;
	}
	public Company getCompany() {
		return company;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public HashMap<String, Boolean> getActions() {
		return actions;
	}
	public String getToken() {
		return token;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public void setActions(HashMap<String, Boolean> actions) {
		this.actions = actions;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
