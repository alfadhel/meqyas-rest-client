package info.meqyas.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class Offer implements Serializable {
	private long id;
	private Product product;
	private Company company;
	private double price;
	private Currency currency;
	private Date fromDate;
	private Date toDate;
	private List<Product> additionalProducts;
	private List<Branch> branchs;
	private String defaultLang;
	private OfferLocal local;
	public Offer() {
	}
	public long getId() {
		return id;
	}
	public Product getProduct() {
		return product;
	}
	public Company getCompany() {
		return company;
	}
	public double getPrice() {
		return price;
	}
	public Currency getCurrency() {
		return currency;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public List<Product> getAdditionalProducts() {
		return additionalProducts;
	}
	public List<Branch> getBranchs() {
		return branchs;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public OfferLocal getLocal() {
		return local;
	}
	public void set_id(long id) {
		this.id = id;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public void setAdditionalProducts(List<Product> additionalProducts) {
		this.additionalProducts = additionalProducts;
	}
	public void setBranchs(List<Branch> branchs) {
		this.branchs = branchs;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public void setLocal(OfferLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Offer other = (Offer) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
