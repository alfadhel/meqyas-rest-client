package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RoleAction implements Serializable {
	private long _id;
	private Role role;
	private Action action;
	private Boolean value;
	public RoleAction() {
	}
	public long get_id() {
		return _id;
	}
	public Role getRole() {
		return role;
	}
	public Action getAction() {
		return action;
	}
	public Boolean getValue() {
		return value;
	}
	public void set_id(long id) {
		_id = id;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public void setAction(Action action) {
		this.action = action;
	}
	public void setValue(Boolean value) {
		this.value = value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleAction other = (RoleAction) obj;
		if(_id == other._id)
			return true;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}
}
