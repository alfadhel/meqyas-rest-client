package info.meqyas.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Content implements Serializable {
	private long id;
	private Account author;
	private String title;
	private String imgUrl;
	private Date date = new Date();
	private ContentLocal local;
	private String instance;
	public Content() {
	}
	public long getId() {
		return id;
	}
	public Account getAuthor() {
		return author;
	}
	public String getTitle() {
		return title;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public Date getDate() {
		return date;
	}
	public ContentLocal getLocal() {
		return local;
	}
	public String getInstance(){
		instance = "Content";
		if(this instanceof News)
			instance = "News";
		else if(this instanceof ProductReview)
			instance = "Review";
		return instance;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setAuthor(Account author) {
		this.author = author;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setLocal(ContentLocal local) {
		this.local = local;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Content other = (Content) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
