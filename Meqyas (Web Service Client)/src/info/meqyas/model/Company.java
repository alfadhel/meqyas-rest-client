package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Company implements Serializable {
	private long id;
	private String name;
	private String defaultLang;
	private CompanyLocal local;
	public Company() {
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public CompanyLocal getLocal() {
		return local;
	}
	public void set_id(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public void setLocal(CompanyLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (id != other.id)
			return false;
		return true;
	}
}