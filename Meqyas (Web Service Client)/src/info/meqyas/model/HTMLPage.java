package info.meqyas.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class HTMLPage implements Serializable {
	private long id;
	private String name;
	private String url;
	private String defaultLang;
	private Account creator;
	private Date createDate;
	private HTMLPageLocal local; 
	public HTMLPage() {
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getUrl() {
		return url;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public Account getCreator() {
		return creator;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public HTMLPageLocal getLocal() {
		return local;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public void setCreator(Account creator) {
		this.creator = creator;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public void setLocal(HTMLPageLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HTMLPage other = (HTMLPage) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
