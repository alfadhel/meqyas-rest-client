package info.meqyas.model;

@SuppressWarnings("serial")
public class ProductReview extends Content {
	private Product product;
	public ProductReview() {
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
}
