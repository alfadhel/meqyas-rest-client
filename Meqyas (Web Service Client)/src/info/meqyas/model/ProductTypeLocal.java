package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProductTypeLocal implements Serializable {
	private String lang;
	private String name;
	public ProductTypeLocal() {
	}
	public String getLang() {
		return lang;
	}
	public String getName() {
		return name;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductTypeLocal other = (ProductTypeLocal) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		return true;
	}
}
