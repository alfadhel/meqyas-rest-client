package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Currency implements Serializable {
	private String code;
	private String name;
	private String defaultLang;
	private CurrencyLocal local;
	public Currency() {
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public CurrencyLocal getLocal() {
		return local;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public void setLocal(CurrencyLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}
}
