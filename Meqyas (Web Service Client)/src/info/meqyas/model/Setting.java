package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Setting implements Serializable {
	private String key;
	private SettingGroup group;
	private int order;
	private String inputType;
	private String inputValues;
	private String value;
	private String dataType;
	private String unit;
	public Setting() {
	}
	public String getKey() {
		return key;
	}
	public SettingGroup getGroup() {
		return group;
	}
	public int getOrder() {
		return order;
	}
	public String getInputType() {
		return inputType;
	}
	public String getInputValues() {
		return inputValues;
	}
	public String getValue() {
		return value;
	}
	public String getDataType() {
		return dataType;
	}
	public String getUnit() {
		return unit;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public void setGroup(SettingGroup group) {
		this.group = group;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	public void setInputValues(String inputValues) {
		this.inputValues = inputValues;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Setting other = (Setting) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
}
