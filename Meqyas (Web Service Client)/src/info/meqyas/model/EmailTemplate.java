package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class EmailTemplate implements Serializable {
	private long id;
	private String name;
	private String defaultLang;
	private EmailTemplateLocal local;
	public EmailTemplate() {
	}
	public long getId() {
		return id;
	}
	public void setId(long _id) {
		this.id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public EmailTemplateLocal getLocal() {
		return local;
	}
	public void setLocal(EmailTemplateLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailTemplate other = (EmailTemplate) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
