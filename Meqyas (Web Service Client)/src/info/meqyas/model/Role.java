package info.meqyas.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Role implements Serializable {
	private long id;
	private String name;
	private List<RoleAction> actions;
	public Role() {
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public List<RoleAction> getActions() {
		return actions;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setActions(List<RoleAction> actions) {
		this.actions = actions;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
