package info.meqyas.api;

import info.meqyas.api.RestClient.HttpVerb;
import info.meqyas.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.api.Wrapper.ResultWrapperForOffer;
import info.meqyas.api.Wrapper.ResultWrapperForOfferList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.Branch;
import info.meqyas.model.Currency;
import info.meqyas.model.Offer;
import info.meqyas.model.OfferLocal;
import info.meqyas.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

class ClientOffer {
	private String apiKey;
	public ClientOffer(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public Offer addOffer(String token, Offer offer) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(offer == null)
			throw new NullPointerException();
		OfferLocal local = offer.getLocal();
		if(local == null)
			local = new OfferLocal();
		Product product = offer.getProduct();
		if(product == null)
			product = new Product();
		Currency currency =offer.getCurrency();
		if(currency == null)
			currency = new Currency();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("product_id", product.getId()));
		postData.add(new RestPostParameter("price", offer.getPrice()));
		postData.add(new RestPostParameter("currency_code", currency.getCode()));
		postData.add(new RestPostParameter("fromDate", offer.getFromDate()));
		postData.add(new RestPostParameter("toDate", offer.getToDate()));
		postData.add(new RestPostParameter("defaultLang", offer.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", token));
		postData.add(new RestPostParameter("description", token));
		if(offer.getBranchs() != null){
			for(Branch branch : offer.getBranchs()){
				postData.add(new RestPostParameter("branch", branch.getId()));
			}
		}
		if(offer.getAdditionalProducts() != null){
			for(Product addProduct : offer.getAdditionalProducts()){
				postData.add(new RestPostParameter("additionalProduct", addProduct.getId()));
			}
		}
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOffer resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOffer.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Offer editOffer(String token, Offer offer, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(offer == null)
			throw new NullPointerException();
		OfferLocal local = offer.getLocal();
		if(local == null)
			local = new OfferLocal();
		Product product = offer.getProduct();
		if(product == null)
			product = new Product();
		Currency currency =offer.getCurrency();
		if(currency == null)
			currency = new Currency();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", offer.getId()));
		postData.add(new RestPostParameter("product_id", product.getId()));
		postData.add(new RestPostParameter("price", offer.getPrice()));
		postData.add(new RestPostParameter("currency_code", currency.getCode()));
		postData.add(new RestPostParameter("fromDate", offer.getFromDate()));
		postData.add(new RestPostParameter("toDate", offer.getToDate()));
		postData.add(new RestPostParameter("defaultLang", offer.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", token));
		postData.add(new RestPostParameter("description", token));
		if(offer.getBranchs() != null){
			for(Branch branch : offer.getBranchs()){
				postData.add(new RestPostParameter("branch", branch.getId()));
			}
		}
		if(offer.getAdditionalProducts() != null){
			for(Product addProduct : offer.getAdditionalProducts()){
				postData.add(new RestPostParameter("additionalProduct", addProduct.getId()));
			}
		}
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOffer resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOffer.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteOffer(String token, Offer offer) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(offer == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", offer.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Offer getOffer(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOffer resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOffer.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Offer> getOfferList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOfferList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOfferList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Offer> getOfferListByCompany(String token, long companyId, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionList")+"ByCompany/"+companyId);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOfferList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOfferList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Offer> getOfferListByProduct(String token, long productId, String localLang, int lastIndex, int count) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer") + ConfigurationManager.AppSettings("ActionList")+"ByProduct/"+productId);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&last_index=%d&count=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, lastIndex, count));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOfferList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOfferList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
