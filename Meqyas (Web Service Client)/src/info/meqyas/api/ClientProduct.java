package info.meqyas.api;

import info.meqyas.api.RestClient.HttpVerb;
import info.meqyas.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.api.Wrapper.ResultWrapperForProduct;
import info.meqyas.api.Wrapper.ResultWrapperForProductList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.Product;
import info.meqyas.model.ProductFeature;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

class ClientProduct {
	private String apiKey;
	public ClientProduct(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public Product addProduct(String token, Product product) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(product == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProduct") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("type_id", product.getType().getId()));
		postData.add(new RestPostParameter("brand", product.getBrand().getId()));
		postData.add(new RestPostParameter("name", product.getName()));
		postData.add(new RestPostParameter("modelNo", product.getModelNo()));
		postData.add(new RestPostParameter("number", product.getNumber()));
		//Features
		for (ProductFeature productFeature : product.getFeatures()) {
			if(productFeature.getFeature().getInputType().equalsIgnoreCase("checkbox")){
				String[] values = productFeature.getValue().split(",");
				for (String value : values) {
					postData.add(new RestPostParameter("feature_" + productFeature.getFeature().getId(), value));
				}
			}else{
				postData.add(new RestPostParameter("feature_" + productFeature.getFeature().getId(), productFeature.getValue()));
			}
		}
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProduct resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProduct.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Product editProduct(String token, Product product, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(product == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProduct") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", product.getId()));
		postData.add(new RestPostParameter("brand", product.getBrand().getId()));
		postData.add(new RestPostParameter("name", product.getName()));
		postData.add(new RestPostParameter("modelNo", product.getModelNo()));
		postData.add(new RestPostParameter("number", product.getNumber()));
		//Features
		for (ProductFeature productFeature : product.getFeatures()) {
			postData.add(new RestPostParameter("feature_" + productFeature.getFeature().getId() + "_id", productFeature.getFeature().getId()));
			if(productFeature.getFeature().getInputType().equalsIgnoreCase("checkbox")){
				String[] values = productFeature.getValue().split(",");
				for (String value : values) {
					postData.add(new RestPostParameter("feature_" + productFeature.getFeature().getId() + "_value", value));
				}
			}else{
				postData.add(new RestPostParameter("feature_" + productFeature.getFeature().getId() + "_value", productFeature.getValue()));
			}
		}
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProduct resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProduct.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteProduct(String token, Product product) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(product == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProduct") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", product.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteProductPhoto(String token, long photoId) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(photoId == 0)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProduct") + ConfigurationManager.AppSettings("ActionDelete")+"Photo");
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", photoId));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Product getProduct(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProduct") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProduct resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProduct.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Product> getProductList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProduct") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProductList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProductList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
