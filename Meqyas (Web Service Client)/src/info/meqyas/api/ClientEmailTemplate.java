package info.meqyas.api;

import info.meqyas.api.RestClient.HttpVerb;
import info.meqyas.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.api.Wrapper.ResultWrapperForEmailTemplate;
import info.meqyas.api.Wrapper.ResultWrapperForEmailTemplateList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.EmailTemplateLocal;
import info.meqyas.model.EmailTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

class ClientEmailTemplate {
	private String apiKey;
	public ClientEmailTemplate(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public EmailTemplate addEmailTemplate(String token, EmailTemplate template) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(template == null)
			throw new NullPointerException();
		EmailTemplateLocal local = template.getLocal();
		if(local == null)
			local = new EmailTemplateLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceEmailTemplate") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("name", template.getName()));
		postData.add(new RestPostParameter("defaultLang", template.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("local_name", local.getName()));
		postData.add(new RestPostParameter("local_title", local.getTitle()));
		postData.add(new RestPostParameter("local_body", local.getBody()));
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForEmailTemplate resultWrapper = gson.fromJson(jsonStr, ResultWrapperForEmailTemplate.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public EmailTemplate editEmailTemplate(String token, EmailTemplate template, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(template == null)
			throw new NullPointerException();
		EmailTemplateLocal local = template.getLocal();
		if(local == null)
			local = new EmailTemplateLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceEmailTemplate") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", template.getId()));
		postData.add(new RestPostParameter("name", template.getName()));
		postData.add(new RestPostParameter("defaultLang", template.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("local_name", local.getName()));
		postData.add(new RestPostParameter("local_title", local.getTitle()));
		postData.add(new RestPostParameter("local_body", local.getBody()));
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForEmailTemplate resultWrapper = gson.fromJson(jsonStr, ResultWrapperForEmailTemplate.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteEmailTemplate(String token, EmailTemplate template) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(template == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceEmailTemplate") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", template.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public EmailTemplate getEmailTemplate(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceEmailTemplate") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForEmailTemplate resultWrapper = gson.fromJson(jsonStr, ResultWrapperForEmailTemplate.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<EmailTemplate> getEmailTemplateList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceEmailTemplate") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForEmailTemplateList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForEmailTemplateList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
