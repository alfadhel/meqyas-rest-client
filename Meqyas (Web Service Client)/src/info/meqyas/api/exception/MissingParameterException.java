package info.meqyas.api.exception;

/**
 * 
 * @author abdullah ibrahim
 *
 */
public class MissingParameterException extends Exception {
	private static final long serialVersionUID = -3217864191303665161L;
	public MissingParameterException(String msg) {
		super(msg);
	}
}
