package info.meqyas.api.exception;

public class NotFoundException extends Exception {
	private static final long serialVersionUID = -971897800473139755L;
	public NotFoundException(String msg){
		super(msg);
	}
}
