package info.meqyas.api.exception;

public class AccountNotFoundException extends Exception {
	private static final long serialVersionUID = 5610710012001475679L;
	public AccountNotFoundException() {
		super("Account not found");
	}
}
