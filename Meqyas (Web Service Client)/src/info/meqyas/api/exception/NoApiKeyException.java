package info.meqyas.api.exception;

public class NoApiKeyException extends Exception {
	private static final long serialVersionUID = 2484342498747971652L;
	public NoApiKeyException() {
		super("API Key (apiKey) not found. Please add it in 'api.config' file");
	}
}
