package info.meqyas.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class SettingGroup implements Serializable {
	private String name;
	private int order;
	private List<Setting> settings;
	public SettingGroup() {
	}
	public String getName() {
		return name;
	}
	public int getOrder() {
		return order;
	}
	public List<Setting> getSettings() {
		return settings;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public void setSettings(List<Setting> settings) {
		this.settings = settings;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SettingGroup other = (SettingGroup) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
