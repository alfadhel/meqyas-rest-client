package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProductPoll implements Serializable {
	private long id;
	private Product product;
	private Account voter;
	private int design;
	private int features;
	private int performance;
	public ProductPoll(){}
	public long getId() {
		return id;
	}
	public Product getProduct() {
		return product;
	}
	public Account getVoter() {
		return voter;
	}
	public int getDesign() {
		return design;
	}
	public int getFeatures() {
		return features;
	}
	public int getPerformance() {
		return performance;
	}
	public void setId(long _id) {
		this.id = _id;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public void setVoter(Account voter) {
		this.voter = voter;
	}
	public void setDesign(int design) {
		this.design = design;
	}
	public void setFeatures(int features) {
		this.features = features;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductPoll other = (ProductPoll) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
