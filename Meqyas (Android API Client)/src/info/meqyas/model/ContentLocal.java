package info.meqyas.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ContentLocal implements Serializable {
	private String lang;
	private String title;
	private String head;
	private String body;
	private List<String> tagList;
	public ContentLocal() {
	}
	public String getLang() {
		return lang;
	}
	public String getTitle() {
		return title;
	}
	public String getHead() {
		return head;
	}
	public String getBody() {
		return body;
	}
	public List<String> getTagList() {
		return tagList;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public void setBody(String body) {
		this.body = body;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContentLocal other = (ContentLocal) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		return true;
	}
}
