package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Feature implements Serializable {
	private long id;
    private String name;
    private String inputType;
	private String inputValues;
	private boolean required;
	private int order;
	private FeatureLocal local;
	public Feature() {
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getInputType() {
		return inputType;
	}
	public String getInputValues() {
		return inputValues;
	}
	public boolean isRequired() {
		return required;
	}
	public int getOrder() {
		return order;
	}
	public FeatureLocal getLocal() {
		return local;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	public void setInputValues(String inputValues) {
		this.inputValues = inputValues;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public void setLocal(FeatureLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feature other = (Feature) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
