package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class HTMLPageLocal implements Serializable {
	private String lang;
	private String title;
	private String htmlTitle;
	private String content;
	private String style;
	private String script;
	public HTMLPageLocal() {
	}
	public String getLang() {
		return lang;
	}
	public String getTitle() {
		return title;
	}
	public String getHtmlTitle() {
		return htmlTitle;
	}
	public String getContent() {
		return content;
	}
	public String getStyle() {
		return style;
	}
	public String getScript() {
		return script;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setHtmlTitle(String htmlTitle) {
		this.htmlTitle = htmlTitle;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public void setScript(String script) {
		this.script = script;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HTMLPageLocal other = (HTMLPageLocal) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		return true;
	}
}
