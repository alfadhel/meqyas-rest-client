package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ProductFeature implements Serializable {
	private long id;
    private Feature feature;
    private String value;
	public ProductFeature() {
	}
	public long getId() {
		return id;
	}
	public Feature getFeature() {
		return feature;
	}
	public String getValue() {
		return value;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductFeature other = (ProductFeature) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
