package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Branch implements Serializable {
	private long id;
    private Company company;
    private String location;
    private String defaultLang;
    private BranchLocal local;
	public Branch() {
	}
	public long getId() {
		return id;
	}
	public Company getCompany() {
		return company;
	}
	public String getLocation() {
		return location;
	}
	public String getDefaultLang() {
		return defaultLang;
	}
	public BranchLocal getLocal() {
		return local;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public void setDefaultLang(String defaultLang) {
		this.defaultLang = defaultLang;
	}
	public void setLocal(BranchLocal local) {
		this.local = local;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
