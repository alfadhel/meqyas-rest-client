package info.meqyas.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Product implements Serializable {
	private long id;
    private String name;
    private String modelNo;
    private String number;
    private ProductType type;
    private Brand brand;
    private List<ProductFeature> features;
	public Product() {
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getModelNo() {
		return modelNo;
	}
	public String getNumber() {
		return number;
	}
	public ProductType getType() {
		return type;
	}
	public Brand getBrand() {
		return brand;
	}
	public List<ProductFeature> getFeatures() {
		return features;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public void setType(ProductType type) {
		this.type = type;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	public void setFeatures(List<ProductFeature> features) {
		this.features = features;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
