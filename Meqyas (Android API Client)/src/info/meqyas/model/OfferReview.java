package info.meqyas.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class OfferReview implements Serializable {
	private long id;
	private String lang;
	private Account writer;
	private Date date = new Date();
	private String title;
	private String content;
	// out of 5
	private int rating;
	public OfferReview() {
	}
	public long getId() {
		return id;
	}
	public String getLang() {
		return lang;
	}
	public Account getWriter() {
		return writer;
	}
	public Date getDate() {
		return date;
	}
	public String getTitle() {
		return title;
	}
	public String getContent() {
		return content;
	}
	public int getRating() {
		return rating;
	}
	public void setId(long _id) {
		this.id = _id;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public void setWriter(Account writer) {
		this.writer = writer;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfferReview other = (OfferReview) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
