package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FeatureLocal implements Serializable {
	private String lang;
	private String label;
	private String inputLabels;
	public FeatureLocal() {
	}
	public String getLang() {
		return lang;
	}
	public String getLabel() {
		return label;
	}
	public String getInputLabels() {
		return inputLabels;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setInputLabels(String inputLabels) {
		this.inputLabels = inputLabels;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((inputLabels == null) ? 0 : inputLabels.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeatureLocal other = (FeatureLocal) obj;
		if (inputLabels == null) {
			if (other.inputLabels != null)
				return false;
		} else if (!inputLabels.equals(other.inputLabels))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Local [lang=" + lang + ", label=" + label
				+ ", inputValues=" + inputLabels + "]";
	}
}
