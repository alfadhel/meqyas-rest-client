package info.meqyas.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Application implements Serializable {
	private String key;
	private Date date;
	private String name;
	private String description;
	private String website;
	private Account owner;
	public Application() {
	}
	public String getKey() {
		return key;
	}
	public Date getDate() {
		return date;
	}
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public String getWebsite() {
		return website;
	}
	public Account getOwner() {
		return owner;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public void setOwner(Account owner) {
		this.owner = owner;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
}
