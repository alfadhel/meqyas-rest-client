package info.meqyas.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Action implements Serializable {
	private String name;
	private ActionGroup group;
	private Boolean defaultValue;
	private int order;
	public Action() {
	}
	public Action(String name, ActionGroup group, Boolean defaultValue,
			int order) {
		this.name = name;
		this.group = group;
		this.defaultValue = defaultValue;
		this.order = order;
	}
	public Action(String name, Boolean defaultValue,
			int order) {
		this.name = name;
		this.defaultValue = defaultValue;
		this.order = order;
	}
	public String getName() {
		return name;
	}
	public ActionGroup getGroup() {
		return group;
	}
	public Boolean getDefaultValue() {
		return defaultValue;
	}
	public int getOrder() {
		return order;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setGroup(ActionGroup group) {
		this.group = group;
	}
	public void setDefaultValue(Boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Action other = (Action) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
