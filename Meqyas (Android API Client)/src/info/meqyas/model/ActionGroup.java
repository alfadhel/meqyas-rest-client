package info.meqyas.model;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ActionGroup implements Serializable {
	private String name;
	private int order;
	private List<Action> actions;
	public ActionGroup() {
	}
	public ActionGroup(String name, int order) {
		super();
		this.name = name;
		this.order = order;
	}
	public String getName() {
		return name;
	}
	public int getOrder() {
		return order;
	}
	public List<Action> getActions() {
		return actions;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionGroup other = (ActionGroup) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
