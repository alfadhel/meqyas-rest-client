package info.meqyas.android.api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * 
 * @author abdullah ibrahim
 *
 */
class RestClient {
	/**
	 * 
	 * @author abdullah ibrahim
	 *
	 */
	public enum HttpVerb{
		GET, POST, PUT, DELETE
	}
	
	public String endPoint;
	public HttpVerb method;
	public String contentType;
	public List<RestPostParameter> postData;
	public String accept;
	
	public RestClient() {
		this.method = HttpVerb.GET;
		this.contentType = "text/html";
		this.accept = "application/json";
	}
	
	public RestClient(String endPoint){
		this.endPoint = endPoint;
		this.method = HttpVerb.GET;
		this.contentType = "text/html";
		this.accept = "application/json";
	}
	
	public RestClient(String endPoint, HttpVerb method){
		this.endPoint = endPoint;
		this.method = method;
		this.contentType = "text/html";
		this.accept = "application/json";
	}
	
	public RestClient(String endPoint, HttpVerb method, List<RestPostParameter> postData){
		this.endPoint = endPoint;
		this.method = method;
		this.postData = postData;
		this.contentType = "text/html";
		this.accept = "application/json";
	}
	
	public String makeRequest() throws Exception{
		return makeRequest("");
	}
	
	public String makeRequest(String parameters) throws Exception{
		URL url = new URL(this.endPoint+parameters);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("Content-Type", this.contentType);
		conn.setRequestMethod(this.method.toString());
		conn.setRequestProperty("accept", this.accept);
		if(this.method == HttpVerb.POST && postData != null){
			conn.setDoOutput(true);
			StringBuilder input = new StringBuilder();
			for (RestPostParameter parameter : postData) {
				if(parameter.value == null)
					continue;
				if(input.length() != 0)
					input.append("&");
				input.append(parameter.name).append("=").append(parameter.value);
			}
			OutputStream os = conn.getOutputStream();
			os.write(input.toString().getBytes("UTF-8"));
			os.flush();
		}
		try {
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String message = String.format("Request failed. Recevied HTTP %d", conn.getResponseCode());
				throw new Exception(message);
			}
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			conn.disconnect();
			return builder.toString();
		} catch (Exception e) {
			throw e;
		}
	}
}
