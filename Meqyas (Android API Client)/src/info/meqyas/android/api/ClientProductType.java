package info.meqyas.android.api;

import info.meqyas.android.api.RestClient.HttpVerb;
import info.meqyas.android.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.android.api.Wrapper.ResultWrapperForProductType;
import info.meqyas.android.api.Wrapper.ResultWrapperForProductTypeList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.Feature;
import info.meqyas.model.ProductType;
import info.meqyas.model.ProductTypeLocal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

class ClientProductType {
	private String apiKey;
	public ClientProductType(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public ProductType addProductType(String token, ProductType type) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(type == null)
			throw new NullPointerException();
		ProductTypeLocal local = type.getLocal();
		if(local == null)
			local = new ProductTypeLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProductType") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("name", type.getName()));
		postData.add(new RestPostParameter("defaultLang", type.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("local_name", local.getName()));
		//
		for (Feature feature : type.getFeatures()) {
			postData.add(new RestPostParameter("feature_order", feature.getOrder()));
			postData.add(new RestPostParameter("feature_name", feature.getName()));
			postData.add(new RestPostParameter("feature_inputType", feature.getInputType()));
			if(feature.getInputType().equalsIgnoreCase("select") || 
					feature.getInputType().equalsIgnoreCase("radio") || 
					feature.getInputType().equalsIgnoreCase("checkbox")){
				postData.add(new RestPostParameter("feature_inputValues", feature.getInputValues()));
				postData.add(new RestPostParameter("local_feature_input_labels", feature.getLocal().getInputLabels()));
			}
			postData.add(new RestPostParameter("feature_required", feature.isRequired()));
			postData.add(new RestPostParameter("local_feature_label", feature.getLocal().getLabel()));
		}
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProductType resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProductType.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public ProductType editProductType(String token, ProductType type, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(type == null)
			throw new NullPointerException();
		ProductTypeLocal local = type.getLocal();
		if(local == null)
			local = new ProductTypeLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProductType") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", type.getId()));
		postData.add(new RestPostParameter("name", type.getName()));
		postData.add(new RestPostParameter("defaultLang", type.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("local_name", local.getName()));
		//
		for (Feature feature : type.getFeatures()) {
			postData.add(new RestPostParameter("feature_id", feature.getId()));
			postData.add(new RestPostParameter("feature_order", feature.getOrder()));
			postData.add(new RestPostParameter("feature_name", feature.getName()));
			postData.add(new RestPostParameter("feature_inputType", feature.getInputType()));
			if(feature.getInputType().equalsIgnoreCase("select") || 
					feature.getInputType().equalsIgnoreCase("radio") || 
					feature.getInputType().equalsIgnoreCase("checkbox")){
				postData.add(new RestPostParameter("feature_inputValues", feature.getInputValues()));
				postData.add(new RestPostParameter("local_feature_input_labels", feature.getLocal().getInputLabels()));
			}
			postData.add(new RestPostParameter("feature_required", feature.isRequired()));
			postData.add(new RestPostParameter("local_feature_label", feature.getLocal().getLabel()));
		}
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProductType resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProductType.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteProductType(String token, ProductType type) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(type == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProductType") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", type.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public ProductType getProductType(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProductType") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProductType resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProductType.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<ProductType> getProductTypeList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceProductType") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProductTypeList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProductTypeList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
