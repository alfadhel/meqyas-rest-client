package info.meqyas.android.api;

import java.util.Properties;

/**
 * 
 * @author abdullah ibrahim
 *
 */
class ConfigurationManager {
	public static String AppSettings(String key){
		Properties prop = new Properties();
		try {
			ConfigurationManager manager = new ConfigurationManager();
			prop.loadFromXML(manager.getClass().getClassLoader().getResourceAsStream("app.config"));
			manager = null;
			return prop.getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
			return key;
		}
	}
	public static String ApiKey(){
		Properties prop = new Properties();
		try {
			ConfigurationManager manager = new ConfigurationManager();
			prop.loadFromXML(manager.getClass().getClassLoader().getResourceAsStream("api.config"));
			manager = null;
			return prop.getProperty("apiKey");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
