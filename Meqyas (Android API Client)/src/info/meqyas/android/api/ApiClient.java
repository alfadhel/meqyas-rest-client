package info.meqyas.android.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.gson.Gson;

import info.meqyas.android.api.RestClient.HttpVerb;
import info.meqyas.android.api.Wrapper.*;
import info.meqyas.api.exception.*;
import info.meqyas.model.*;
import info.meqyas.util.Form;

/**
 * 
 * @author abdullah ibrahim
 *
 */
public class ApiClient {
	private String apiKey;
	/**
	 * 
	 * @throws Exception if apiKey not found
	 */
	public ApiClient(Context context) throws NoApiKeyException {
		try {
			ApplicationInfo info = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = info.metaData;
			this.apiKey = bundle.getString("info.meqyas.android.api.API_KEY");
		} catch (Exception e) {
			throw new NoApiKeyException();
		}
		if(this.apiKey == null)
			throw new NoApiKeyException();
	}
	/**
	 * 
	 * @param screenName ScreenName or known as username.
	 * @param password plain text
	 * @return account information
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public Account authenticate(String screenName, String password) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceLogin"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("screenName", screenName));
		postData.add(new RestPostParameter("password", password));
		client.postData = postData;
		client.method = HttpVerb.POST;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s", this.apiKey));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForAccount resultWrapper = gson.fromJson(jsonStr, ResultWrapperForAccount.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw getException(code, result);
		}
	}
	/**
	 * 
	 * @param email
	 * @param screenName
	 * @param password
	 * @param fullName
	 * @param gender
	 * @param timeZone
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public Account Register(String email, String screenName, String password, String fullName, char gender, String timeZone) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceRegister"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("email", email));
		postData.add(new RestPostParameter("screenName", screenName));
		postData.add(new RestPostParameter("password", password));
		postData.add(new RestPostParameter("fullName", fullName));
		postData.add(new RestPostParameter("gender", gender));
		postData.add(new RestPostParameter("language", Locale.getDefault().getLanguage()));
		postData.add(new RestPostParameter("timeZone", timeZone));
		client.postData = postData;
		client.method = HttpVerb.POST;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s", this.apiKey));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForAccount resultWrapper = gson.fromJson(jsonStr, ResultWrapperForAccount.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw getException(code, result);
		}
	}
	/**
	 * 
	 * @param token
	 * @param account
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Account addAccount(String token, Account account) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientAccount(apiKey).addAccount(token, account);
	}
	/**
	 * 
	 * @param token
	 * @param account
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Account editAccount(String token, Account account) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, NotFoundException, Exception{
		return new ClientAccount(apiKey).editAccount(token, account);
	}
	/**
	 * 
	 * @param token
	 * @param account
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public boolean deleteAccount(String token, Account account) throws ApiKeyNotValidException, PrivilegeException, NotFoundException, Exception{
		return new ClientAccount(apiKey).deleteAccount(token, account);
	}
	/**
	 * 
	 * @param screenName
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Account getAccount(String token, String screenName) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientAccount(apiKey).getAccount(token, screenName);
	}
	/**
	 * 
	 * @param token
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Account> getAccountList(String token, String query, int lastIndex, int count, Integer status) throws ApiKeyNotValidException, Exception{
		return new ClientAccount(query).getAccountList(token, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param branch
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Branch addBranch(String token, Branch branch) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientBranch(apiKey).addBranch(token, branch);
	}
	/**
	 * 
	 * @param token
	 * @param branch
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Branch editBranch(String token, Branch branch, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientBranch(apiKey).editBranch(token, branch, status);
	}
	/**
	 * 
	 * @param token
	 * @param branch
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteBranch(String token, Branch branch) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientBranch(apiKey).deleteBranch(token, branch);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Branch getBranch(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientBranch(apiKey).getBranch(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param companyId
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Branch> getBranchList(String token, String localLang, long companyId, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientBranch(apiKey).getBranchList(token, localLang, companyId, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param brand
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Brand addBrand(String token, Brand brand) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientBrand(apiKey).addBrand(token, brand);
	}
	/**
	 * 
	 * @param token
	 * @param brand
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Brand editBrand(String token, Brand brand, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientBrand(apiKey).editBrand(token, brand, status);
	}
	/**
	 * 
	 * @param token
	 * @param brand
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteBrand(String token, Brand brand) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientBrand(apiKey).deleteBrand(token, brand);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Brand getBrand(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientBrand(apiKey).getBrand(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Brand> getBrandList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientBrand(apiKey).getBrandList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param company
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Company addCompany(String token, Company company) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientCompany(apiKey).addCompany(token, company);
	}
	/**
	 * 
	 * @param token
	 * @param company
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Company editCompany(String token, Company company, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientCompany(apiKey).editCompany(token, company, status);
	}
	/**
	 * 
	 * @param token
	 * @param company
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteCompany(String token, Company company) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientCompany(apiKey).deleteCompany(token, company);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Company getCompany(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientCompany(apiKey).getCompany(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Company> getCompanyList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientCompany(apiKey).getCompanyList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param currency
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Currency addCurrency(String token, Currency currency) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientCurrency(apiKey).addCurrency(token, currency);
	}
	/**
	 * 
	 * @param token
	 * @param currency
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Currency editCurrency(String token, Currency currency, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientCurrency(apiKey).editCurrency(token, currency, status);
	}
	/**
	 * 
	 * @param token
	 * @param currency
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteCurrency(String token, Currency currency) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientCurrency(apiKey).deleteCurrency(token, currency);
	}
	/**
	 * 
	 * @param token
	 * @param code
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Currency getCurrency(String token, String code, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientCurrency(apiKey).getCurrency(token, code, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Currency> getCurrencyList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientCurrency(apiKey).getCurrencyList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param template
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public EmailTemplate addEmailTemplate(String token, EmailTemplate template) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientEmailTemplate(apiKey).addEmailTemplate(token, template);
	}
	/**
	 * 
	 * @param token
	 * @param template
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public EmailTemplate editEmailTemplate(String token, EmailTemplate template, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientEmailTemplate(apiKey).editEmailTemplate(token, template, status);
	}
	/**
	 * 
	 * @param token
	 * @param template
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteEmailTemplate(String token, EmailTemplate template) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientEmailTemplate(apiKey).deleteEmailTemplate(token, template);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public EmailTemplate getEmailTemplate(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientEmailTemplate(apiKey).getEmailTemplate(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<EmailTemplate> getEmailTemplateList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientEmailTemplate(apiKey).getEmailTemplateList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param page
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public HTMLPage addHTMLPage(String token, HTMLPage page) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientHTMLPage(apiKey).addHTMLPage(token, page);
	}
	/**
	 * 
	 * @param token
	 * @param page
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public HTMLPage editHTMLPage(String token, HTMLPage page, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientHTMLPage(apiKey).editHTMLPage(token, page, status);
	}
	/**
	 * 
	 * @param token
	 * @param page
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteHTMLPage(String token, HTMLPage page) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientHTMLPage(apiKey).deleteHTMLPage(token, page);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public HTMLPage getHTMLPage(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientHTMLPage(apiKey).getHTMLPage(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<HTMLPage> getHTMLPageList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientHTMLPage(apiKey).getHTMLPageList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param news
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public News addNews(String token, News news) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientNews(apiKey).addNews(token, news);
	}
	/**
	 * 
	 * @param token
	 * @param news
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public News editNews(String token, News news, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientNews(apiKey).editNews(token, news, status);
	}
	/**
	 * 
	 * @param token
	 * @param news
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteNews(String token, News news) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientNews(apiKey).deleteNews(token, news);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public News getNews(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientNews(apiKey).getNews(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<News> getNewsList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientNews(apiKey).getNewsList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param offer
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Offer addOffer(String token, Offer offer) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientOffer(apiKey).addOffer(token, offer);
	}
	/**
	 * 
	 * @param token
	 * @param offer
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Offer editOffer(String token, Offer offer, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientOffer(apiKey).editOffer(token, offer, status);
	}
	/**
	 * 
	 * @param token
	 * @param offer
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteOffer(String token, Offer offer) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientOffer(apiKey).deleteOffer(token, offer);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Offer getOffer(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientOffer(apiKey).getOffer(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Offer> getOfferList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientOffer(apiKey).getOfferList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param companyId
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Offer> getOfferListByCompany(String token, long companyId, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientOffer(apiKey).getOfferListByCompany(token, companyId, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param productId
	 * @param localLang
	 * @param lastIndex
	 * @param count
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Offer> getOfferListByProduct(String token, long productId, String localLang, int lastIndex, int count) throws ApiKeyNotValidException, Exception{
		return new ClientOffer(apiKey).getOfferListByProduct(token, productId, localLang, lastIndex, count);
	}
	/**
	 * 
	 * @param token
	 * @param offerReview
	 * @param offerId
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public OfferReview addOfferReview(String token, OfferReview offerReview, long offerId) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientOfferReview(apiKey).addOfferReview(token, offerReview, offerId);
	}
	/**
	 * 
	 * @param token
	 * @param offerReview
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public OfferReview editOfferReview(String token, OfferReview offerReview, int status) throws ApiKeyNotValidException, Exception{
		return new ClientOfferReview(apiKey).editOfferReview(token, offerReview, status);
	}
	/**
	 * 
	 * @param token
	 * @param offerReview
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteOfferReview(String token, OfferReview offerReview) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientOfferReview(apiKey).deleteOfferReview(token, offerReview);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public OfferReview getOfferReview(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientOfferReview(apiKey).getOfferReview(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param offerId
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<OfferReview> getOfferReviewList(String token, long offerId, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientOfferReview(apiKey).getOfferReviewList(token, offerId, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param product
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Product addProduct(String token, Product product) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProduct(apiKey).addProduct(token, product);
	}
	/**
	 * 
	 * @param token
	 * @param product
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Product editProduct(String token, Product product, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProduct(apiKey).editProduct(token, product, status);
	}
	/**
	 * 
	 * @param token
	 * @param product
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteProduct(String token, Product product) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProduct(apiKey).deleteProduct(token, product);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Product getProduct(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientProduct(apiKey).getProduct(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Product> getProductList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientProduct(apiKey).getProductList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param review
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public ProductReview addProductReview(String token, ProductReview review) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProductReview(apiKey).addProductReview(token, review);
	}
	/**
	 * 
	 * @param token
	 * @param review
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public ProductReview editProductReview(String token, ProductReview review, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProductReview(apiKey).editProductReview(token, review, status);
	}
	/**
	 * 
	 * @param token
	 * @param review
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteProductReview(String token, ProductReview review) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProductReview(apiKey).deleteProductReview(token, review);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public ProductReview getProductReview(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientProductReview(apiKey).getProductReview(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<ProductReview> getProductReviewList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientProductReview(apiKey).getProductReviewList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param type
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public ProductType addProductType(String token, ProductType type) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProductType(apiKey).addProductType(token, type);
	}
	/**
	 * 
	 * @param token
	 * @param type
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public ProductType editProductType(String token, ProductType type, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProductType(apiKey).editProductType(token, type, status);
	}
	/**
	 * 
	 * @param token
	 * @param type
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public boolean deleteProductType(String token, ProductType type) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientProductType(apiKey).deleteProductType(token, type);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public ProductType getProductType(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientProductType(apiKey).getProductType(token, id, localLang, status);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<ProductType> getProductTypeList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		return new ClientProductType(apiKey).getProductTypeList(token, localLang, query, lastIndex, count, status);
	}
	/**
	 * 
	 * @param token
	 * @param role
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Role addRole(String token, Role role) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientRole(apiKey).addRole(token, role);
	}
	/**
	 * 
	 * @param token
	 * @param role
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Role editRole(String token, Role role) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientRole(apiKey).editRole(token, role);
	}
	/**
	 * 
	 * @param token
	 * @param id
	 * @param localLang
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Role getRole(String token, long id, String localLang) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientRole(apiKey).getRole(token, id, localLang);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastIndex
	 * @param count
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Role> getRoleList(String token, String localLang, String query, int lastIndex, int count) throws ApiKeyNotValidException, Exception{
		return new ClientRole(apiKey).getRoleList(token, localLang, query, lastIndex, count);
	}
	/**
	 * 
	 * @param token
	 * @param setting
	 * @param status
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws PrivilegeException
	 * @throws MissingParameterException
	 * @throws InvalidParameterException
	 * @throws Exception
	 */
	public Setting editSetting(String token, Setting setting, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		return new ClientSetting(apiKey).editSetting(token, setting, status);
	}
	/**
	 * 
	 * @param token
	 * @param key
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public Setting getSetting(String token, String key) throws ApiKeyNotValidException, NotFoundException, Exception{
		return new ClientSetting(apiKey).getSetting(token, key);
	}
	/**
	 * 
	 * @param token
	 * @param localLang
	 * @param query
	 * @param lastOrder
	 * @param count
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Setting> getSettingList(String token, String localLang, String query, long lastOrder, int count) throws ApiKeyNotValidException, Exception{
		return new ClientSetting(apiKey).getSettingList(token, localLang, query, lastOrder, count);
	}
	/**
	 * 
	 * @param query
	 * @param count
	 * @param last_id
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Product> serachForProduct(String query, int count, long last_id) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceSearch") + ConfigurationManager.AppSettings("ObjectProduct"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&last_index=%d&lang=%s&count=%d&query=%s",
				this.apiKey, last_id, Locale.getDefault().getLanguage(), count, query));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForProductList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForProductList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw getException(code, result);
		}
	}
	/**
	 * 
	 * @param product_id
	 * @param count
	 * @param last_id
	 * @return
	 * @throws ApiKeyNotValidException
	 * @throws Exception
	 */
	public List<Offer> getProductOffers(long product_id, int count, long last_id) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceOffer"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&product_id=%d&last_index=%d&lang=%s&count=%d",
				this.apiKey, product_id, last_id, Locale.getDefault().getLanguage(), count));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForOfferList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForOfferList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw getException(code, result);
		}
	}
	
	public Form getRoleForm() throws Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				"/ServiceForm" + ConfigurationManager.AppSettings("ActionAdd") + "/Role");
		client.method = HttpVerb.POST;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForForm resultWrapper = gson.fromJson(jsonStr, ResultWrapperForForm.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw getException(code, result);
		}
	}
	
	protected static Exception getException(int code, String result){
		switch (code) {
		case 403:
			return new ApiKeyNotValidException();
		case 1002:
			
		case 1003:
			return new PrivilegeException(result);
		case 1004:
			return new MissingParameterException(result);
		case 1005:
			return new InvalidParameterException(result);
		case 1006:
			return new NotFoundException(result);
		default:
			return new Exception(result);
		}
	}
}
