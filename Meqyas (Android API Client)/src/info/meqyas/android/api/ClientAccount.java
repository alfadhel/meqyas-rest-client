package info.meqyas.android.api;

import info.meqyas.android.api.RestClient.HttpVerb;
import info.meqyas.android.api.Wrapper.ResultWrapperForAccount;
import info.meqyas.android.api.Wrapper.ResultWrapperForAccountList;
import info.meqyas.android.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.Account;
import info.meqyas.model.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

class ClientAccount {
	private String apiKey;
	public ClientAccount(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public Account addAccount(String token, Account account) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(account == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceAccount") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("screenName", account.getScreenName()));
		postData.add(new RestPostParameter("fullName", account.getFullName()));
		postData.add(new RestPostParameter("gender", account.getGender()));
		postData.add(new RestPostParameter("email", account.getEmail()));
		postData.add(new RestPostParameter("timeZone", account.getTimeZone()));
		postData.add(new RestPostParameter("active", true));
		postData.add(new RestPostParameter("delete", false));
		if(account.getRoles() != null)
		for (Role role : account.getRoles()) {
			postData.add(new RestPostParameter("role", role.getId()));
		}
		if(account.getCompany()!=null)
			postData.add(new RestPostParameter("company_id", account.getCompany().getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForAccount resultWrapper = gson.fromJson(jsonStr, ResultWrapperForAccount.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Account editAccount(String token, Account account) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, NotFoundException, Exception{
		if(account == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceAccount") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("screenName", account.getScreenName()));
		postData.add(new RestPostParameter("fullName", account.getFullName()));
		postData.add(new RestPostParameter("gender", account.getGender()));
		postData.add(new RestPostParameter("email", account.getEmail()));
		postData.add(new RestPostParameter("timeZone", account.getTimeZone()));
		postData.add(new RestPostParameter("active", true));
		postData.add(new RestPostParameter("delete", false));
		if(account.getRoles() != null)
		for (Role role : account.getRoles()) {
			postData.add(new RestPostParameter("role", role.getId()));
		}
		if(account.getCompany()!=null)
			postData.add(new RestPostParameter("company_id", account.getCompany().getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForAccount resultWrapper = gson.fromJson(jsonStr, ResultWrapperForAccount.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteAccount(String token, Account account) throws ApiKeyNotValidException, PrivilegeException, NotFoundException, Exception{
		if(account == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceAccount") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("screenName", account.getScreenName()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Account getAccount(String token, String screenName) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceAccount") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + screenName);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") , this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForAccount resultWrapper = gson.fromJson(jsonStr, ResultWrapperForAccount.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Account> getAccountList(String token, String query, int lastIndex, int count, Integer status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceAccount") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForAccountList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForAccountList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
