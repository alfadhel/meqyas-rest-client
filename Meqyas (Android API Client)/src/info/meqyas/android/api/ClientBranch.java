package info.meqyas.android.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

import info.meqyas.android.api.RestClient.HttpVerb;
import info.meqyas.android.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.android.api.Wrapper.ResultWrapperForBranch;
import info.meqyas.android.api.Wrapper.ResultWrapperForBranchList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.Branch;
import info.meqyas.model.BranchLocal;

class ClientBranch {
	private String apiKey;
	public ClientBranch(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public Branch addBranch(String token, Branch branch) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(branch == null)
			throw new NullPointerException();
		BranchLocal local = branch.getLocal();
		if(local == null)
			local = new BranchLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBranch") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("location", branch.getLocation()));
		postData.add(new RestPostParameter("defaultLang", branch.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("name", local.getName()));
		postData.add(new RestPostParameter("description", local.getDescription()));
		postData.add(new RestPostParameter("contact", local.getContact()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBranch resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBranch.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Branch editBranch(String token, Branch branch, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(branch == null)
			throw new NullPointerException();
		BranchLocal local = branch.getLocal();
		if(local == null)
			local = new BranchLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBranch") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", branch.getId()));
		postData.add(new RestPostParameter("location", branch.getLocation()));
		postData.add(new RestPostParameter("defaultLang", branch.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("name", local.getName()));
		postData.add(new RestPostParameter("description", local.getDescription()));
		postData.add(new RestPostParameter("contact", local.getContact()));
		postData.add(new RestPostParameter("status", status));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBranch resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBranch.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteBranch(String token, Branch branch) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(branch == null)
			throw new NullPointerException();
		BranchLocal local = branch.getLocal();
		if(local == null)
			local = new BranchLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBranch") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", branch.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Branch getBranch(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBranch") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBranch resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBranch.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Branch> getBranchList(String token, String localLang, long companyId, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBranch") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&company_id=%d&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, companyId, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBranchList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBranchList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
