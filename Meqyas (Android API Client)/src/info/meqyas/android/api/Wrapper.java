package info.meqyas.android.api;

import java.util.List;

import info.meqyas.model.*;
import info.meqyas.util.Form;

class Wrapper {
	public static class ResultWrapperForBoolean{
		private boolean status;
    	private int code;
    	private boolean result;
    	public ResultWrapperForBoolean() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public boolean getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(boolean result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForAccount{
    	private boolean status;
    	private int code;
    	private Account result;
    	public ResultWrapperForAccount() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Account getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Account result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForAccountList{
    	private boolean status;
    	private int code;
    	private List<Account> result;
    	public ResultWrapperForAccountList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Account> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Account> result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForBranch{
    	private boolean status;
    	private int code;
    	private Branch result;
    	public ResultWrapperForBranch() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Branch getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Branch result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForBranchList{
    	private boolean status;
    	private int code;
    	private List<Branch> result;
    	public ResultWrapperForBranchList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Branch> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Branch> result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForBrand{
    	private boolean status;
    	private int code;
    	private Brand result;
    	public ResultWrapperForBrand() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Brand getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Brand result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForBrandList{
    	private boolean status;
    	private int code;
    	private List<Brand> result;
    	public ResultWrapperForBrandList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Brand> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Brand> result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForCompany{
    	private boolean status;
    	private int code;
    	private Company result;
    	public ResultWrapperForCompany() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Company getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Company result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForCompanyList{
    	private boolean status;
    	private int code;
    	private List<Company> result;
    	public ResultWrapperForCompanyList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Company> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Company> result) {
			this.result = result;
		}
    }
	public static class ResultWrapperForCurrency{
		private boolean status;
    	private int code;
    	private Currency result;
    	public ResultWrapperForCurrency() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Currency getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Currency result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForCurrencyList{
		private boolean status;
    	private int code;
    	private List<Currency> result;
    	public ResultWrapperForCurrencyList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Currency> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Currency> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForEmailTemplate{
		private boolean status;
    	private int code;
    	private EmailTemplate result;
    	public ResultWrapperForEmailTemplate() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public EmailTemplate getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(EmailTemplate result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForEmailTemplateList{
		private boolean status;
    	private int code;
    	private List<EmailTemplate> result;
    	public ResultWrapperForEmailTemplateList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<EmailTemplate> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<EmailTemplate> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForHTMLPage{
		private boolean status;
    	private int code;
    	private HTMLPage result;
    	public ResultWrapperForHTMLPage() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public HTMLPage getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(HTMLPage result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForHTMLPageList{
		private boolean status;
    	private int code;
    	private List<HTMLPage> result;
    	public ResultWrapperForHTMLPageList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<HTMLPage> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<HTMLPage> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForNews{
		private boolean status;
    	private int code;
    	private News result;
    	public ResultWrapperForNews() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public News getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(News result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForNewsList{
		private boolean status;
    	private int code;
    	private List<News> result;
    	public ResultWrapperForNewsList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<News> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<News> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForOffer{
		private boolean status;
    	private int code;
    	private Offer result;
    	public ResultWrapperForOffer() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Offer getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Offer result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForOfferList{
		private boolean status;
    	private int code;
    	private List<Offer> result;
    	public ResultWrapperForOfferList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Offer> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Offer> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForOfferReview{
		private boolean status;
    	private int code;
    	private OfferReview result;
    	public ResultWrapperForOfferReview() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public OfferReview getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(OfferReview result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForOfferReviewList{
		private boolean status;
    	private int code;
    	private List<OfferReview> result;
    	public ResultWrapperForOfferReviewList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<OfferReview> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<OfferReview> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForProduct{
		private boolean status;
    	private int code;
    	private Product result;
    	public ResultWrapperForProduct() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Product getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Product result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForProductList{
		private boolean status;
    	private int code;
    	private List<Product> result;
    	public ResultWrapperForProductList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Product> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Product> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForProductReview{
		private boolean status;
    	private int code;
    	private ProductReview result;
    	public ResultWrapperForProductReview() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public ProductReview getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(ProductReview result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForProductReviewList{
		private boolean status;
    	private int code;
    	private List<ProductReview> result;
    	public ResultWrapperForProductReviewList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<ProductReview> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<ProductReview> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForProductType{
		private boolean status;
    	private int code;
    	private ProductType result;
    	public ResultWrapperForProductType() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public ProductType getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(ProductType result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForProductTypeList{
		private boolean status;
    	private int code;
    	private List<ProductType> result;
    	public ResultWrapperForProductTypeList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<ProductType> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<ProductType> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForRole{
		private boolean status;
    	private int code;
    	private Role result;
    	public ResultWrapperForRole() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Role getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Role result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForRoleList{
		private boolean status;
    	private int code;
    	private List<Role> result;
    	public ResultWrapperForRoleList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Role> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Role> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForSetting{
		private boolean status;
    	private int code;
    	private Setting result;
    	public ResultWrapperForSetting() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Setting getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Setting result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForSettingList{
		private boolean status;
    	private int code;
    	private List<Setting> result;
    	public ResultWrapperForSettingList() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public List<Setting> getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(List<Setting> result) {
			this.result = result;
		}
	}
	public static class ResultWrapperForForm{
		private boolean status;
    	private int code;
    	private Form result;
    	public ResultWrapperForForm() { }
		public boolean isStatus() {
			return status;
		}
    	public int getCode() {
			return code;
		}
    	public Form getResult() {
			return result;
		}
    	public void setStatus(boolean status) {
			this.status = status;
		}
    	public void setCode(int code) {
			this.code = code;
		}
    	public void setResult(Form result) {
			this.result = result;
		}
	}
}
