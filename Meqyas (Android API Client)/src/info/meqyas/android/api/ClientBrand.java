package info.meqyas.android.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

import info.meqyas.android.api.RestClient.HttpVerb;
import info.meqyas.android.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.android.api.Wrapper.ResultWrapperForBrand;
import info.meqyas.android.api.Wrapper.ResultWrapperForBrandList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.Brand;
import info.meqyas.model.BrandLocal;

class ClientBrand {
	private String apiKey;
	public ClientBrand(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public Brand addBrand(String token, Brand brand) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(brand == null)
			throw new NullPointerException();
		BrandLocal local = brand.getLocal();
		if(local == null)
			local = new BrandLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBrand") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("name", local.getName()));
		postData.add(new RestPostParameter("defaultLang", brand.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("local_name", local.getName()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBrand resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBrand.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Brand editBrand(String token, Brand brand, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(brand == null)
			throw new NullPointerException();
		BrandLocal local = brand.getLocal();
		if(local == null)
			local = new BrandLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBrand") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", brand.getId()));
		postData.add(new RestPostParameter("name", local.getName()));
		postData.add(new RestPostParameter("defaultLang", brand.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("local_name", local.getName()));
		postData.add(new RestPostParameter("status", status));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBrand resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBrand.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteBrand(String token, Brand brand) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(brand == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBrand") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", brand.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public Brand getBrand(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBrand") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBrand resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBrand.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<Brand> getBrandList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceBrand") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBrandList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBrandList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}	
	}
}
