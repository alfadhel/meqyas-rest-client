package info.meqyas.android.api;

import info.meqyas.android.api.RestClient.HttpVerb;
import info.meqyas.android.api.Wrapper.ResultWrapperForBoolean;
import info.meqyas.android.api.Wrapper.ResultWrapperForHTMLPage;
import info.meqyas.android.api.Wrapper.ResultWrapperForHTMLPageList;
import info.meqyas.api.exception.ApiKeyNotValidException;
import info.meqyas.api.exception.InvalidParameterException;
import info.meqyas.api.exception.MissingParameterException;
import info.meqyas.api.exception.NotFoundException;
import info.meqyas.api.exception.PrivilegeException;
import info.meqyas.model.HTMLPage;
import info.meqyas.model.HTMLPageLocal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.gson.Gson;

class ClientHTMLPage {
	private String apiKey;
	public ClientHTMLPage(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public HTMLPage addHTMLPage(String token, HTMLPage page) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(page == null)
			throw new NullPointerException();
		HTMLPageLocal local = page.getLocal();
		if(local == null)
			local = new HTMLPageLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceHTMLPage") + ConfigurationManager.AppSettings("ActionAdd"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("name", page.getName()));
		postData.add(new RestPostParameter("url", page.getUrl()));
		postData.add(new RestPostParameter("defaultLang", page.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("title", local.getTitle()));
		postData.add(new RestPostParameter("htmlTitle", local.getHtmlTitle()));
		postData.add(new RestPostParameter("content", local.getContent()));
		postData.add(new RestPostParameter("style", local.getStyle()));
		postData.add(new RestPostParameter("script", local.getScript()));
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForHTMLPage resultWrapper = gson.fromJson(jsonStr, ResultWrapperForHTMLPage.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public HTMLPage editHTMLPage(String token, HTMLPage page, int status) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(page == null)
			throw new NullPointerException();
		HTMLPageLocal local = page.getLocal();
		if(local == null)
			local = new HTMLPageLocal();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceHTMLPage") + ConfigurationManager.AppSettings("ActionEdit"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", page.getId()));
		postData.add(new RestPostParameter("name", page.getName()));
		postData.add(new RestPostParameter("url", page.getUrl()));
		postData.add(new RestPostParameter("defaultLang", page.getDefaultLang()));
		postData.add(new RestPostParameter("localLang", local.getLang()));
		postData.add(new RestPostParameter("title", local.getTitle()));
		postData.add(new RestPostParameter("htmlTitle", local.getHtmlTitle()));
		postData.add(new RestPostParameter("content", local.getContent()));
		postData.add(new RestPostParameter("style", local.getStyle()));
		postData.add(new RestPostParameter("script", local.getScript()));
		//
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForHTMLPage resultWrapper = gson.fromJson(jsonStr, ResultWrapperForHTMLPage.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public boolean deleteHTMLPage(String token, HTMLPage page) throws ApiKeyNotValidException, PrivilegeException, MissingParameterException, InvalidParameterException, Exception{
		if(page == null)
			throw new NullPointerException();
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceHTMLPage") + ConfigurationManager.AppSettings("ActionDelete"));
		ArrayList<RestPostParameter> postData = new ArrayList<RestPostParameter>();
		postData.add(new RestPostParameter("token", token));
		postData.add(new RestPostParameter("_id", page.getId()));
		client.postData = postData;
		client.method = HttpVerb.POST;
		client.contentType = "application/x-www-form-urlencoded";
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s", this.apiKey, Locale.getDefault().getLanguage()));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForBoolean resultWrapper = gson.fromJson(jsonStr, ResultWrapperForBoolean.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public HTMLPage getHTMLPage(String token, long id, String localLang, int status) throws ApiKeyNotValidException, NotFoundException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceHTMLPage") + ConfigurationManager.AppSettings("ActionGet") + 
				"/" + id);
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"") 
				+ "&localLang=%s&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForHTMLPage resultWrapper = gson.fromJson(jsonStr, ResultWrapperForHTMLPage.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
	
	public List<HTMLPage> getHTMLPageList(String token, String localLang, String query, int lastIndex, int count, int status) throws ApiKeyNotValidException, Exception{
		RestClient client = new RestClient(ConfigurationManager.AppSettings("EndPoint") + 
				ConfigurationManager.AppSettings("ServiceHTMLPage") + ConfigurationManager.AppSettings("ActionList"));
		client.method = HttpVerb.GET;
		String jsonStr = client.makeRequest(String.format("?apiKey=%s&lang=%s" + (token!=null?"&token="+token:"")  
				+ "&localLang=%s&query=%s&last_index=%d&count=%d&status=%d", this.apiKey, Locale.getDefault().getLanguage(), localLang, query, lastIndex, count, status));
		Gson gson = JsonUtil.getBuilder();
		@SuppressWarnings("unchecked")
		Map<String, Object> values = gson.fromJson(jsonStr, Map.class);
		if((Boolean)values.get("status")){
			ResultWrapperForHTMLPageList resultWrapper = gson.fromJson(jsonStr, ResultWrapperForHTMLPageList.class);
			return resultWrapper.getResult();
		}else{
			int code = Double.valueOf(values.get("code").toString()).intValue();
			String result = values.get("result").toString();
			throw ApiClient.getException(code, result);
		}
	}
}
