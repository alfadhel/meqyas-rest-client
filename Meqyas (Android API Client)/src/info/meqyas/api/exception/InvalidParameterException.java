package info.meqyas.api.exception;

/**
 * 
 * @author abdullah ibrahim
 *
 */
public class InvalidParameterException extends Exception {
	private static final long serialVersionUID = -3217864191303665161L;
	public InvalidParameterException(String msg) {
		super(msg);
	}
}
