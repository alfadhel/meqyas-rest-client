package info.meqyas.api.exception;

public class PrivilegeException extends Exception {
	private static final long serialVersionUID = -4313450192347500455L;
	public PrivilegeException(String msg) {
		super(msg);
	}
}
