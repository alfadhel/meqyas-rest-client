package info.meqyas.api.exception;

public class NoApiKeyException extends Exception {
	private static final long serialVersionUID = 2484342498747971652L;
	public NoApiKeyException() {
		super("API Key (info.meqyas.android.api.API_KEY) not found. Please add it in 'AndroidManifest.xml' file");
	}
}
