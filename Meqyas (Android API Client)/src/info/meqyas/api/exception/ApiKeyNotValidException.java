package info.meqyas.api.exception;

public class ApiKeyNotValidException extends Exception {
	private static final long serialVersionUID = 356238543617959595L;
	public ApiKeyNotValidException() {
		super("API Key not valid");
	}
}
